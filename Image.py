from Exceptions import *
import numpy as np

class Image:
	def __init__(self):
		self.image = None
		self.rows = None
		self.columns = None

	#changes the colour of a pixel
	def image_colour(self, x, y, colour):
		if colour.isalpha():
			try:
				self.image[x][y] = colour
			except TypeError:
				print("Not a valid number")
			except IndexError:
				print("Out of bounds")
		else: 
			raise TypeError

	#fills the board with white pixels
	def white(self):
		self.image[:] = 'O'

	#creates a new image
	def create(self, new_rows, new_columns):
		self.rows = new_rows
		self.columns = new_columns
		self.image=np.chararray((self.rows, self.columns))
		self.white()

	#clears the previous image
	def clear(self):
		if self.image is not None: 
			self.white()
		else: 
			raise noImage

	#executes the command for changing the colour of one pixel
	def single_colour(self, x, y, colour):
		self.image_colour(x, y, colour)

	#changes the colour of pixels vertically
	def vertical_colour(self, x, y1, y2, colour):
		if self.image is not None:
			i = y1
			while i <= y2:
				self.image_colour(i, x, colour)
				i+=1
		else: 
			raise noImage

	#changes the colour of pixels horizontally
	def horizontal_colour(self, x1, x2, y, colour):
		if self.image is not None:
			i = x1
			while i <= x2:
				self.image_colour(y, i, colour)
				i+=1
		else: 
			raise noImage

	#changes the colour of one pixel and of all of its adjacent pixels
	def fill(self, x, y, colour):
		if self.image is not None:
			previous_colour = self.image[x][y]
			self.image_colour(x, y, colour)
			if (x - 1) >= 0:
				if self.image[x-1][y] == previous_colour:
					self.image_colour(x-1, y, colour)
			if (x + 1) <= (self.rows - 1):
				if self.image[x+1][y] == previous_colour:
					self.image_colour(x+1,y, colour)
			if (y - 1) >= 0:
				if self.image[x][y-1] == previous_colour:
					self.image_colour(x, y-1, colour)
			if (y + 1) <= (self.columns - 1):
				if self.image[x][y+1] == previous_colour:
					self.image_colour(x, y+1, colour)
			if (x - 1) >= 0 and (y - 1) >= 0:
				if self.image[x-1][y-1] == previous_colour:
					self.image_colour(x-1, y-1, colour)
			if (x - 1) >= 0 and (y + 1) <= (self.columns -1):
				if self.image[x-1][y+1] == previous_colour:
					self.image_colour(x-1, y+1, colour)
			if (x + 1) <= (self.rows - 1) and (y - 1) >= 0:
				if self.image[x+1][y-1] == previous_colour:
					self.image_colour(x+1, y-1, colour)
			if (x + 1) <= (self.rows - 1) and (y + 1) <= (self.columns -1):
				if self.image[x+1][y+1] == previous_colour:
					self.image_colour(x+1, y+1, colour)
		else: 
			raise noImage

	#print the image on the screen
	def image_print(self):
		if self.image is not None:
			i = 0
			j = 0
			output=""
			while i < self.rows:
				while j < self.columns:
					output += str(self.image[i][j]).split('\'')[1]
					j+=1
				j = 0
				i += 1
				print(output)
				output=""
		else:
			raise noImage
