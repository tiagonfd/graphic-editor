from Image import *

#Evaluate input line and execute respective command
def execute(image, command):
	comm = command[0]
	if comm == 'I' and len(command) == 3 and command[1].isdigit() and command[2].isdigit():
		image.create(int(command[1]), int(command[2]))
	elif comm == 'C' and len(command) == 1:
		image.clear()
	elif comm == 'L' and len(command) == 4 and command[1].isdigit() and command[2].isdigit():
		image.single_colour(int(command[1]), int(command[2]), command[3])
	elif comm == 'V' and len(command) == 5 and command[1].isdigit() and command[2].isdigit() and command[3].isdigit():
		image.vertical_colour(int(command[1]), int(command[2]), int(command[3]), command[4])
	elif comm == 'H' and len(command) == 5 and command[1].isdigit() and command[2].isdigit() and command[3].isdigit():
		image.horizontal_colour(int(command[1]), int(command[2]), int(command[3]), command[4])
	elif comm == 'F' and len(command) == 4 and command[1].isdigit() and command[2].isdigit():
		image.fill(int(command[1]), int(command[2]), command[3])
	elif comm == 'S' and len(command) == 1:
		image.image_print()
	else:
		raise invalidInput

def main():
	image = Image()
	while True: 
		command = input(">")
		command = str.split(command)
		execute(image, command)


if __name__ == "__main__":
    main()